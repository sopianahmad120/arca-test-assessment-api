## Langkah-langkah Instalasi

### 1. Kloning Repositori
```bash
git clone https://github.com/username/nama-repositori.git
cd nama-repositori
```
### 2. Konfigurasi environment
```bash
cp .env.example .env
```

### 3. Instalasi dependensi
```bash
composer install
```

### 4. Menjalankan sail dengan docker
```bash
./vendor/bin/sail up -d
```

### 5. Migrasi dan seeding database
```bash
./vendor/bin/sail artisan migrate --seed
```

### 6. Akses aplikasi
```bash
http://localhost
```

### Informasi Login
Gunakan informasi berikut untuk login sebagai super admin:
* Email: superadmin@gmail.com
* Password: Asdf1234
