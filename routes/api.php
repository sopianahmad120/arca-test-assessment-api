<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\BonusController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');

Route::post('/login', [AuthController::class, 'login']);
Route::post('/register', [AuthController::class, 'register']);

Route::middleware(['auth:sanctum'])->group(function () {
    Route::middleware(['role:super-admin'])->group(function () {
        Route::get('/bonuses', [BonusController::class, 'index'])->middleware('can:list bonus');
        Route::post('/bonuses', [BonusController::class, 'store'])->middleware('can:create bonus');
        Route::delete('/bonuses/{id}', [BonusController::class, 'destroy'])->middleware('can:delete bonus');
    });
    Route::post('/logout', [AuthController::class, 'logout']);
});
