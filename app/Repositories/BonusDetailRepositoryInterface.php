<?php

namespace App\Repositories;

use App\Models\Bonus;
use Illuminate\Database\Eloquent\Collection;

interface BonusDetailRepositoryInterface
{
    public function createMany(Bonus $bonus, array $details): Collection;

    public function calculateAmount($totalAmount, $percentage): float|int;
}
