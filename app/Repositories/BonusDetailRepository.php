<?php

namespace App\Repositories;

use App\Models\Bonus;
use Illuminate\Database\Eloquent\Collection;

class BonusDetailRepository implements BonusDetailRepositoryInterface
{
    public function createMany(Bonus $bonus, array $details): Collection
    {
        return $bonus->details()->createMany($details);
    }

    public function calculateAmount($totalAmount, $percentage): float|int
    {
        return ($totalAmount * $percentage) / 100;
    }
}
