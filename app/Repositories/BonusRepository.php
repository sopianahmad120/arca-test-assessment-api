<?php

namespace App\Repositories;

use App\Models\Bonus;

class BonusRepository implements BonusRepositoryInterface
{

    public function create(array $data): Bonus
    {
        return Bonus::create($data);
    }

    public function update(int $id, array $data): void
    {
        // TODO: Implement update() method.
    }

    public function delete(int $id): int
    {
        return Bonus::destroy($id);
    }

    public function find(int $id): array
    {
        return Bonus::findOrFail($id);
    }

    public function all(): array
    {
        return Bonus::orderBy('created_at', 'desc')->paginate(10)->toArray();
    }
}
