<?php

namespace App\Repositories;
use App\Models\Bonus;

interface BonusRepositoryInterface
{
    public function create(array $data): Bonus;
    public function update(int $id, array $data): void;
    public function delete(int $id): int;
    public function find(int $id): array;
    public function all(): array;
}
