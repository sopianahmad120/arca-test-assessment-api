<?php

namespace App\Providers;

use App\Repositories\BonusDetailRepositoryInterface;
use App\Repositories\BonusRepositoryInterface;
use App\Services\BonusService;
use Illuminate\Support\ServiceProvider;

class ServiceServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(BonusService::class, function ($app) {
            return new BonusService(
                $app->make(BonusRepositoryInterface::class),
                $app->make(BonusDetailRepositoryInterface::class)
            );
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
