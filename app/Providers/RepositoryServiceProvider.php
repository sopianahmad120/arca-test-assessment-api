<?php

namespace App\Providers;

use App\Repositories\BonusDetailRepository;
use App\Repositories\BonusDetailRepositoryInterface;
use App\Repositories\BonusRepository;
use App\Repositories\BonusRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(
            BonusRepositoryInterface::class,
            BonusRepository::class
        );

        $this->app->bind(
            BonusDetailRepositoryInterface::class,
            BonusDetailRepository::class
        );
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
