<?php

namespace App\Http\Controllers;

use App\Exceptions\InvalidBonusException;
use App\Http\Requests\StoreBonusRequest;
use App\Models\Bonus;
use App\Models\BonusDetail;
use App\Services\BonusService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\RecordsNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class BonusController extends Controller
{

    public function __construct(
        protected BonusService $bonusService
    ){}
    public function test(Request $request)
    {
        // Create role and permission
//        $role = Role::create(['name' => 'regular']);
//        $permission = Permission::create(['name' => 'view bonuses']);
//        $role->givePermissionTo($permission);

        // Assign role to user
//        $user = auth()->user();
//        $user->assignRole($role);

//        $role = Role::findByName('regular');
//        $role->syncPermissions(['view bonuses', 'delete bonuses', 'edit bonuses']);

//        $request->user()->assignRole('regular');

        // user can view bonuses
        if ($request->user()->can('view bonuses')) {
            return response()->json(['message' => 'Bonuses']);
        }

//        return response()->json(['message' => 'Bonuses']);
    }

    public function index()
    {
        $bonuses = $this->bonusService->getAllBonuses();
        return $this->responseJson('Bonuses retrieved successfully', $bonuses);
    }

    public function store(StoreBonusRequest $request)
    {
        try {
            $this->bonusService->createBonusWithDetails($request->all(), $request->details);
            return $this->responseJson('Bonus created successfully');
        } catch (InvalidBonusException $e) {
            return $this->responseJson($e->getMessage(), success: false, status: 400);
        } catch (\Exception $e) {
            return $this->responseJson('Failed to create bonus', success: false, status: 500);
        }
    }

    public function destroy(int $id)
    {
        try {
            $this->bonusService->deleteBonus($id);
            return $this->responseJson('Bonus deleted successfully');
        } catch (ModelNotFoundException $e) {
            throw new ModelNotFoundException();
        } catch (\Exception $e) {
            return $this->responseJson('Failed to delete bonus', success: false, status: 500);
        }
    }
}
