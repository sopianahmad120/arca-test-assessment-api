<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class StoreBonusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'total_amount' => 'required|numeric',
            'details' => 'required|array|min:3',
            'details.*.name' => 'required|string',
            'details.*.percentage' => 'required|numeric',
        ];
    }

    public function failedValidation(Validator $validator): void
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'message' => 'Validation errors',
            'data' => $validator->errors()
        ], 422));

    }

    public function messages(): array
    {
        return [
            'total_amount.required' => 'The total amount field is required.',
            'total_amount.numeric' => 'The total amount field must be a number.',
            'details.required' => 'The details field is required.',
            'details.array' => 'The details field must be an array.',
            'details.min' => 'The details field must have at least 3 items.',
            'details.*.name.required' => 'The name field is required.',
            'details.*.name.string' => 'The name field must be a string.',
            'details.*.percentage.required' => 'The percentage field is required.',
            'details.*.percentage.numeric' => 'The percentage field must be a number.',
        ];
    }
}
