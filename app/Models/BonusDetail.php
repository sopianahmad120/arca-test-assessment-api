<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class BonusDetail extends Model
{
    use HasFactory;

    protected $fillable = ['bonus_id', 'name', 'amount', 'percentage'];

    public function bonus(): BelongsTo
    {
        return $this->belongsTo(Bonus::class);
    }

    public static function calculateAmount($totalAmount, $percentage): float|int
    {
        return ($totalAmount * $percentage) / 100;
    }
}
