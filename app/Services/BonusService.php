<?php

namespace App\Services;

use App\Exceptions\InvalidBonusException;
use App\Models\Bonus;
use App\Repositories\BonusDetailRepositoryInterface;
use App\Repositories\BonusRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\RecordsNotFoundException;
use Illuminate\Support\Facades\DB;

class BonusService
{
    public function __construct(
        protected BonusRepositoryInterface $bonusRepository,
        protected BonusDetailRepositoryInterface $bonusDetailRepository,
    ){}

    public function createBonusWithDetails(array $data, array $details): Bonus
    {
        DB::beginTransaction();

        try {
            $totalPercentage = collect($details)->sum('percentage');
            if ($totalPercentage !== 100) {
                throw new InvalidBonusException('The total percentage must be 100%');
            }

            $bonus = $this->bonusRepository->create($data);

            $details = collect($details)->map(function ($detail) use ($data, $bonus) {
                return [
                    'name' => $detail['name'],
                    'percentage' => $detail['percentage'],
                    'amount' => $this->bonusDetailRepository->calculateAmount($data['total_amount'], $detail['percentage']),
                    'bonus_id' => $bonus->id
                ];
            })->toArray();

            $this->bonusDetailRepository->createMany($bonus, $details);

            DB::commit();
            return $bonus;
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function getBonus(int $id): array
    {
        return $this->bonusRepository->find($id);
    }

    public function getAllBonuses(): array
    {
        return $this->bonusRepository->all();
    }

    public function deleteBonus(int $id): int
    {
        $bonus = $this->bonusRepository->find($id);
        $bonus->delete();
    }
}
